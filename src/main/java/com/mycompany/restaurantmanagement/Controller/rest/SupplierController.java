package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.Supplier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/supplier")
public class SupplierController extends CRUDController<Supplier> {
    
   public SupplierController(){
       activeMenu="master";
       viewPath ="/supplier";
       pageURI="supplier";
   }
    
}


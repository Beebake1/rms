package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.PurchaseRequestItem;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@RestController
@RequestMapping("/purchaserequestitem")
public class PurchaseRequestItemController extends CRUDController<PurchaseRequestItem> {
    
   public PurchaseRequestItemController(){
        activeMenu="master";
       viewPath ="/PurchaseRequestItem";
       pageURI="purchaserequestitem";
   }
    
}


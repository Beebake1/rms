package com.mycompany.restaurantmanagement.Controller.rest;

import  com.mycompany.restaurantmanagement.CoreController.CRUDController;
import  com.mycompany.restaurantmanagement.entity.ItemSupplier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/itemsupplier")
public class ItemSupplierController extends CRUDController<ItemSupplier> {
    
   public ItemSupplierController(){
        activeMenu="master";
       viewPath ="/ItemSupplier";
       pageURI="itemsupplier";
   }
    
}


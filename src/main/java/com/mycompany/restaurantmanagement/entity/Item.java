/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Risab
 */
@Entity
@Table(name = "items")

public class Item implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "itemId")
    private List<ItemCategory> itemCategoryList;
    @JsonIgnore    
    @OneToMany(mappedBy = "itemId")
    private List<ItemSupplier> itemSupplierList;
    @JoinColumn(name = "unit_id", referencedColumnName = "id")
    @JsonIgnore 
    @ManyToOne
    private Unit unitId;
    @JsonIgnore 
    @OneToMany(mappedBy = "itemId")
    private List<PurchaseRequestItem> purchaseRequestItemList;

    public Item() {
    }

    public Item(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ItemCategory> getItemCategoryList() {
        return itemCategoryList;
    }

    public void setItemCategoryList(List<ItemCategory> itemCategoryList) {
        this.itemCategoryList = itemCategoryList;
    }

    public List<ItemSupplier> getItemSupplierList() {
        return itemSupplierList;
    }

    public void setItemSupplierList(List<ItemSupplier> itemSupplierList) {
        this.itemSupplierList = itemSupplierList;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    public List<PurchaseRequestItem> getPurchaseRequestItemList() {
        return purchaseRequestItemList;
    }

    public void setPurchaseRequestItemList(List<PurchaseRequestItem> purchaseRequestItemList) {
        this.purchaseRequestItemList = purchaseRequestItemList;
    }

   
    
}

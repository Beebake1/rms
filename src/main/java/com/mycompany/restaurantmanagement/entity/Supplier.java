/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Risab
 */
@Entity
@Table(name = "suppliers")

public class Supplier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 50)
    @Column(name = "contact")
    private String contact;
    @Size(max = 50)
    @Column(name = "contact_person")
    private String contactPerson;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pan_no")
    private int panNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vat_no")
    private int vatNo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_pan")
    private boolean isPan;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_online")
    private boolean isOnline;
    @JsonIgnore    
    @OneToMany(mappedBy = "supplierId")
    private List<PurchaseRequest> purchaseRequestList;
    @JsonIgnore
    @OneToMany(mappedBy = "supplierId")
    private List<ItemSupplier> itemSupplierList;

    public Supplier() {
    }

    public Supplier(Integer id) {
        this.id = id;
    }

    public Supplier(Integer id, String address, int panNo, int vatNo, boolean isPan, boolean isOnline) {
        this.id = id;
        this.address = address;
        this.panNo = panNo;
        this.vatNo = vatNo;
        this.isPan = isPan;
        this.isOnline = isOnline;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPanNo() {
        return panNo;
    }

    public void setPanNo(int panNo) {
        this.panNo = panNo;
    }

    public int getVatNo() {
        return vatNo;
    }

    public void setVatNo(int vatNo) {
        this.vatNo = vatNo;
    }

    public boolean getIsPan() {
        return isPan;
    }

    public void setIsPan(boolean isPan) {
        this.isPan = isPan;
    }

    public boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public List<PurchaseRequest> getPurchaseRequestList() {
        return purchaseRequestList;
    }

    public void setPurchaseRequestList(List<PurchaseRequest> purchaseRequestList) {
        this.purchaseRequestList = purchaseRequestList;
    }

    public List<ItemSupplier> getItemSupplierList() {
        return itemSupplierList;
    }

    public void setItemSupplierList(List<ItemSupplier> itemSupplierList) {
        this.itemSupplierList = itemSupplierList;
    }


    
}

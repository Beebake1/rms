/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author Risab
 */
@Entity
@Table(name = "purchase_request_items")

public class PurchaseRequestItem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "status")
    private Boolean status;
    @Size(max = 200)
    @Column(name = "remarks")
    private String remarks;
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    @JsonIgnore
    @ManyToOne
    private Item itemId;
    @JoinColumn(name = "purchase_request_id", referencedColumnName = "id")
    @JsonIgnore
    @ManyToOne
    private PurchaseRequest purchaseRequestId;
    @JoinColumn(name = "unit_id", referencedColumnName = "id")
    @JsonIgnore
    @ManyToOne
    private Unit unitId;

    public PurchaseRequestItem() {
    }

    public PurchaseRequestItem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Item getItemId() {
        return itemId;
    }

    public void setItemId(Item itemId) {
        this.itemId = itemId;
    }

    public PurchaseRequest getPurchaseRequestId() {
        return purchaseRequestId;
    }

    public void setPurchaseRequestId(PurchaseRequest purchaseRequestId) {
        this.purchaseRequestId = purchaseRequestId;
    }

    public Unit getUnitId() {
        return unitId;
    }

    public void setUnitId(Unit unitId) {
        this.unitId = unitId;
    }

    
    
}

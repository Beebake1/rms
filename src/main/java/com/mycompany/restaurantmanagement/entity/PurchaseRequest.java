/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author Risab
 */
@Entity
@Table(name = "purchase_requests")

public class PurchaseRequest implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "request_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDate;
    @Size(max = 50)
    @Column(name = "remarks")
    private String remarks;
    @JoinColumn(name = "supplier_id", referencedColumnName = "id")
    @JsonIgnore
    @ManyToOne
    private Supplier supplierId;
    @JsonIgnore
    @OneToMany(mappedBy = "purchaseRequestId")
    private List<PurchaseRequestItem> purchaseRequestItemList;

    public PurchaseRequest() {
    }

    public PurchaseRequest(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Supplier getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Supplier supplierId) {
        this.supplierId = supplierId;
    }

    public List<PurchaseRequestItem> getPurchaseRequestItemList() {
        return purchaseRequestItemList;
    }

    public void setPurchaseRequestItemList(List<PurchaseRequestItem> purchaseRequestItemList) {
        this.purchaseRequestItemList = purchaseRequestItemList;
    }

  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.repository;

import com.mycompany.restaurantmanagement.entity.ItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



/**s
 *
 * @author Risab
 */
@Repository
public interface ItemCategoryRepository extends JpaRepository<ItemCategory, Integer>{
    
  
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.restaurantmanagement.Generator;

/**
 *
 * @author Risab
 */
import com.mycompany.restaurantmanagement.CoreController.SiteController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.mycompany.restaurantmanagement.util.FileHelper;
import com.mycompany.restaurantmanagement.util.TextParser;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.nio.file.Files.write;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Risab
 */
@Controller
@RequestMapping("/generator")
public class Generator extends SiteController{
    private String packagePath="com/mycompany/restaurantmanagement/";
    private String generatorPath="./src/main/java/"+packagePath;
    
    public Generator(){
        pageURI="/generator";
    }
    
    @GetMapping()
    public String index(Model model){
        File file=new File(generatorPath +"/entity/");
        model.addAttribute("files", file.list());
        return "generators/index";
    }
    
     @PostMapping()
    public  String generate(@RequestParam("entity") String entity){
        String fileName=generatorPath +"/entity/" +entity;
        File file=new File(fileName);
        String RepositoryPath=generatorPath+"/repository/";
        String TemplatePath="./templates/";
        if(file.exists()){
            try{
            String fileTokens[]=entity.split("\\.");
            String templateContent=FileHelper.readToEnd(TemplatePath+"Repository.tpl");
            generateRepository(fileTokens, TemplatePath);
             generateController(fileTokens, TemplatePath);
                
        }catch(Exception e){
            
        }
        }
        return "generators/index";
    }
   
    public void generateRepository(String[] fileTokens, String TemplatePath) throws IOException{
        String RepositoryPath=generatorPath+"/repository/";
        
        TextParser parser=new TextParser();
           String 
                content = parser.maspData("packageName", packagePath.replace("/", ".")).maspData("Entity", fileTokens[0]).Parse(FileHelper.readToEnd(TemplatePath+ "Repository.tpl"));
            
            
                System.out.println(content);
               FileWriter writer = new FileWriter(RepositoryPath+fileTokens[0]+"Repository.java");
               writer.write(content);
               writer.close();
               
               
    }
    
      public void generateController(String[] fileTokens, String TemplatePath) throws IOException{
        String controllerPath=generatorPath+"/Controller/rest/";
        
        TextParser parser=new TextParser();
           String 
                content = parser.maspData("packageName", packagePath.replace("/", ".")).maspData("Entity", fileTokens[0]).maspData("url",fileTokens[0].toLowerCase()).Parse(FileHelper.readToEnd(TemplatePath+ "Controller.tpl"));
            
            
                System.out.println(content);
                
                FileHelper.write(controllerPath+fileTokens[0]+"Controller.java", content);
                System.out.println("ControllerGenerated");
    }
}


